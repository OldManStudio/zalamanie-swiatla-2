#include<windows.h>
#include<cmath>
#include<ctime>
#include<cstdlib>
#include<sstream>
using namespace std;
#define PROZNIA 1.0
#define POWIETRZE 1.0003
#define WODA 1.33

// Funkcja do debugowania
void Log(HWND &hwnd, double val, int x=200, int y=0)
{
	HDC hdc = GetDC(hwnd);
	ostringstream ss;
	ss << val;
	string str = ss.str();
	TextOut(hdc, x, y, str.c_str(), str.length());
	ReleaseDC(hwnd, hdc);
}
double rad(double number)
{
    return (number*M_PI)/180;
}
POINT oblicz(double angle, double pointX, double pointY,
			 double centerX = 0, double centerY = 0)
{
    angle = rad(angle);
    pointX -= centerX;
    pointY -= centerY;
 
    POINT turnedPoint;
    turnedPoint.x = pointX*cos(angle)-pointY*sin(angle);
    turnedPoint.y = pointX*sin(angle)+pointY*cos(angle);
 
    turnedPoint.x += centerX;
    turnedPoint.y += centerY;
 
    return turnedPoint;
}


/* This is where all the input to the window goes to */
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	switch(Message) {
		
		/* Upon destruction, tell the main thread to stop */
		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}
		
		case WM_COMMAND: {
			switch(wParam)
			{
				case 1: {
					HDC hdc = GetDC(hwnd);
					HPEN pen = CreatePen(PS_SOLID, 2, RGB(255, 255, 0));
					HPEN stary = (HPEN)SelectObject(hdc, pen);
					
					MoveToEx(hdc, 0, 0, NULL);
					LineTo(hdc, 350, 350);
					
					// Za�amanie
					double kat = 45;
					double sinb = (PROZNIA*sin(rad(kat)))/POWIETRZE;
					double roznica = 200;
					double beta = 0;
					// Obliczanie beta
					for(double i=1; i<91; i++)
					{
						double temp = sin(rad(i));
						double roznica2 = abs(sinb-temp);
						if(roznica2 < roznica)
						{
							roznica = roznica2;
							beta = i;
						}
					}
					POINT point = oblicz(45+90+(90-beta), 0, 0, 275, 275);
					MoveToEx(hdc, 350, 350, NULL);
					LineTo(hdc, point.x, point.y);
					
					
					
					// Za�amanie 2
					kat = beta;
					double sinb2 = (sin(rad(kat)))/1.33;
					double roznica2 = 200;
					double beta2 = 0;
					// Obliczanie beta
					for(double i=1; i<91; i++)
					{
						double temp = sin(rad(i));
						double roznica3 = abs(sinb2-temp);
						if(roznica3 < roznica2)
						{
							roznica2 = roznica3;
							beta2 = i;
						}
					}
					POINT point2 = oblicz(kat+90+(90-beta2), 350, 350, point.x, point.y);
//					MoveToEx(hdc, 350, 350, NULL);
					LineTo(hdc, point2.x, point2.y);
					
					SelectObject(hdc, stary);
					ReleaseDC(hwnd, hdc);
					break;
				}
				case 2: {
					HDC hdc = GetDC(hwnd);
					
					HBRUSH brush = CreateSolidBrush(RGB(0, 0, 0));
					HBRUSH stary = (HBRUSH)SelectObject(hdc, brush);
					Rectangle(hdc, -1, -1, 781, 681);
					
					for(int i=0; i<300; i++)
					{
						SetPixel(hdc, rand()%780, rand()%680, RGB(255, 255, 255));
					}
					
					brush = CreateSolidBrush(RGB(200, 200, 200));
					stary = (HBRUSH)SelectObject(hdc, brush);
					Rectangle(hdc, 0, 350, 780, 780);
					
					brush = CreateSolidBrush(RGB(0, 0, 235));
					SelectObject(hdc, brush);
					Rectangle(hdc, 0, 550, 780, 780);
					
					SelectObject(hdc, stary);
					ReleaseDC(hwnd, hdc);
					break;
				}
				case 3: {
					MessageBox(hwnd,
						"Program korzysta ze wzoru:\n\n\tsin(alfa)/sin(beta) = n2/n1\n\nWe wzorze alfa oznacza k�t padania promienia �wietlnego na powierzchni�, a k�t beta jest k�tem odbicia. Z kolei n1 i n2 s� sta�ymi, kt�re oznaczaj� bezwzgl�dny wsp�czynnik za�amania �wiat�a.\nW programie u�yto trzech o�rodk�w przez kt�re �wiat�o musi przej��. Pierwszym z nich jest pr�nia, kt�rej wsp�czynnik za�amania �wiat�a wynosi 1. Nast�pnie �wiat�o dochodzi do atmosfery ziemskiej, czyli powietrza, kt�rej wsp�czynnik za�amania jest r�wny 1,003. Przez to, �e r�nica wsp�czynnika w tych dw�ch o�rodkach jest niewielka �wiat�o prawie nie ulega za�amaniu (jest ono minimalne)\nNast�pn� przeszkod� dla �wiat�a jest woda (n=1,33). Tutaj ju� wsp�czynnik za�amania widocznie r�ni si�. Dzi�ki temu k�t beta jest wi�kszy i co za tym idzie za�amanie �wiat�a pomi�dzy tymi dwoma o�rodkami r�wnie� jest zauwa�alnie wi�ksze.\n\nPocz�tkowy k�t padania wynosi 45 stopni. Natomiast k�t odbicia jest obliczany poprzez u�o�enie proporcji do powy�szego wzoru i wyznaczeniu z niego sinusa k�ta beta. Dzi�ki temu mo�liwe jest obliczenie k�ta beta.", 
						"Wzory", MB_OK|MB_ICONINFORMATION);
					break;
				}
				case 4: {
					MessageBox(hwnd,
						"Imi� i nazwisko:  Piotr Paul\nKlasa: 3TD\nTemat projektu: Za�amanie �wiat�a",
						"Autor", MB_OK|MB_ICONINFORMATION);
					break;
				}
			}	
			break;
		}
		
		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}

/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	srand(time(NULL));
	WNDCLASSEX wc; /* A properties struct of our window */
	HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
	MSG msg; /* A temporary location for all messages */

	/* zero out the struct and set the stuff we want to modify */
	memset(&wc,0,sizeof(wc));
	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.lpfnWndProc	 = WndProc; /* This is where we will send messages to */
	wc.hInstance	 = hInstance;
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	
	/* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+3);
	wc.lpszClassName = "WindowClass";
	wc.hIcon		 = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION); /* use the name "A" to use the project icon */

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,"WindowClass","Fizyka komputerowa - za�amanie �wiat�a",WS_VISIBLE|WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, /* x */
		CW_USEDEFAULT, /* y */
		780, /* width */
		780, /* height */
		NULL,LoadMenu(hInstance, MAKEINTRESOURCE(50)),hInstance,NULL);

	HDC hdc = GetDC(hwnd);
	
	for(int i=0; i<300; i++)
	{
		SetPixel(hdc, rand()%780, rand()%680, RGB(255, 255, 255));
	}
	
	HBRUSH brush = CreateSolidBrush(RGB(200, 200, 200));
	HBRUSH stary = (HBRUSH)SelectObject(hdc, brush);
	Rectangle(hdc, 0, 350, 780, 780);
	
	brush = CreateSolidBrush(RGB(0, 0, 235));
	SelectObject(hdc, brush);
	Rectangle(hdc, 0, 550, 780, 780);
	
	
	SelectObject(hdc, stary);
	ReleaseDC(hwnd, hdc);

	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	/*
		This is the heart of our program where all input is processed and 
		sent to WndProc. Note that GetMessage blocks code flow until it receives something, so
		this loop will not produce unreasonably high CPU usage
	*/
	while(GetMessage(&msg, NULL, 0, 0) > 0) { /* If no error is received... */
		TranslateMessage(&msg); /* Translate key codes to chars if present */
		DispatchMessage(&msg); /* Send it to WndProc */
	}
	return msg.wParam;
}
